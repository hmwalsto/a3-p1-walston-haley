//
//  TVC.swift
//  A3_P1_Walston_Haley
//
//  Created by Haley Walston on 10/2/19.
//  Copyright © 2019 Haley Walston. All rights reserved.
//

import Foundation
import UIKit


class TVC: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    var lapStatsArray: [String] = []
    var lapsArray: [[(title: String, time: String)]] = [[],[]]
    var sectionHeaders = ["Overall Stats","Lap-wise Stats"]

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionHeaders[section]
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lapsArray[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "My Table", for: indexPath)
        let cellData: (title: String, time: String) = lapsArray[indexPath[0]][indexPath[1]]
        
        // Configure the cell...
        cell.textLabel?.text = cellData.title
        cell.detailTextLabel?.text = cellData.time
        
        return cell
    }
}
