//
//  V2.swift
//  A3_P1_Walston_Haley
//
//  Created by Haley Walston
//  Copyright © 2019 Haley Walston. All rights reserved.
//

import Foundation
import UIKit

class V2: UIViewController{
    
    @IBOutlet weak var currentlapVal: UILabel!
    @IBOutlet weak var totaltimeVal: UILabel!
    @IBOutlet weak var numberlapsVal: UILabel!
    @IBOutlet weak var newLapButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var resetButton: UIBarButtonItem!
    @IBOutlet weak var showStatsButton: UIBarButtonItem!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "SendDataSegue",
            let SendDataSegue = segue.destination as? TVC {
                SendDataSegue.lapsArray = lapsArray
           //    SendDataSegue.lapStatsArray = lapStatsArray
        }
    }
    
    
    var newLapNow = false
    var counter1 = 0
    var counter2 = 0
    var lapCount = 0
    var savedLapTime = ""
    
    var intervalsArray: [Int] = []
    var lapStatsArray: [String] = []
    var lapsArray: [[(String,String)]] = []
    var tempArray: [(String, String)] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        newLapButton.isUserInteractionEnabled = false //disables new lap button on launch
        showStatsButton.isEnabled = false
        resetButton.isEnabled = false
    }

 
    //Timer
    var timer1 = Timer()    //create timer
    var isTimerRunning = false
    
    //Formats the time for the display label
    func formatTime(whichCounter:Int) -> String{
        let mseconds = whichCounter % 10
        let seconds = (whichCounter / 10) % 60
        let minutes = (whichCounter / 600) % 60
        
        return String(format:"%01i:%02i.%01i", minutes, seconds, mseconds)
    }
    
    //starts the timer when called
    @objc func runTimer1(){
        timer1 = Timer.scheduledTimer(timeInterval: 0.1, target: self,   selector: (#selector(updateTimer1)), userInfo: nil, repeats: true)
    }
    
    //Method Timer Calls
    @objc func updateTimer1() {
        counter1 += 1
        counter2 += 1
        
        if newLapNow == true { //new lap button was tapped
            if lapCount < 2 { // first iteration, save the totals
                intervalsArray.append(counter1)
                savedLapTime = formatTime(whichCounter: counter1)
                tempArray.append(("Lap \(lapCount - 1):", "\(savedLapTime)"))
            } else {    //every other iteration
                intervalsArray.append(counter2)
                savedLapTime = formatTime(whichCounter: counter2) 
                tempArray.append(("Lap \(lapCount - 1):", "\(savedLapTime)"))
            }
            counter2 = 0 //resets the current lap to 0
            newLapNow = false
            // - current lap is completed and added to the TVC
        }
        
        //Print timer to labels
        switch (lapCount) {
            case 1:
                currentlapVal.text = formatTime(whichCounter: counter1)
                totaltimeVal.text = formatTime(whichCounter: counter1)
            default: //print timers when laps > 0
                currentlapVal.text = formatTime(whichCounter: counter2)
                totaltimeVal.text = formatTime(whichCounter: counter1)
        }
    }
    
    func scanArray(array: [Int]) -> (arrayMin: String, arrayMax: String, arrayAv: String) {
        let arrayMin = formatTime(whichCounter: intervalsArray.min()!)
        let arrayMax = formatTime(whichCounter: intervalsArray.max()!)
        let sumArray = intervalsArray.reduce(0, +)
        var arrayAv = String( sumArray / intervalsArray.count )
        arrayAv = formatTime(whichCounter: Int(arrayAv)!)
    return (arrayMin, arrayMax, arrayAv)
    }
    
    
    var lapMin = ""
    var lapMax = ""
    var lapAv = ""
    
    @IBAction func startButtonTap(_ sender: Any) {
        if isTimerRunning == false{ //first iteration -- start button tapped
            showStatsButton.isEnabled = true
            newLapButton.isUserInteractionEnabled = true
            resetButton.isEnabled = true
            runTimer1()
            isTimerRunning = true
            startButton.setImage(UIImage(named:"button_stop"), for: .normal)
            
            resetButton.isEnabled = true
            showStatsButton.isEnabled = true
            lapCount = 1
            numberlapsVal.text = String( lapCount )
            
        } else if isTimerRunning == true { //stop button tapped
            timer1.invalidate()
            isTimerRunning = false
            newLapButton.isUserInteractionEnabled = false
            startButton.isEnabled = false
            
            savedLapTime = formatTime(whichCounter: counter2)
            intervalsArray.append(counter2)
            tempArray.append(("Lap \(lapCount):", "\(savedLapTime)"))
            
            let stats = scanArray(array: intervalsArray)
            lapStatsArray = [stats.arrayMin, stats.arrayMax, stats.arrayAv]
            finalizingArray()
        }
    }
    
    @IBAction func newlapButtonTap(_ sender: Any) {
            newLapNow = true
            lapCount += 1
            numberlapsVal.text = String( lapCount ) //print lap count to label
    }
    
    
    @IBAction func resetButtonTap(_ sender: Any) {
        let alert = UIAlertController(title: "WAIT", message: "Are you sure you want to reset?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            self.currentlapVal.text = "0:00.0"
            self.totaltimeVal.text = "0:00.0"
            self.numberlapsVal.text = "0"
            self.counter1 = 0
            self.counter2 = 0
            self.lapCount = 0
            self.lapsArray = []
            self.tempArray = []
            self.isTimerRunning = false
            self.newLapNow = false
            self.newLapButton.isUserInteractionEnabled = false
            self.startButton.setImage(UIImage(named:"button_start"), for: .normal)
            self.startButton.isEnabled = true
            self.showStatsButton.isEnabled = false
            self.timer1.invalidate()
        })
        alert.addAction(yesAction)
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func finalizingArray(){
        lapMin = lapStatsArray[0]
        lapMax = lapStatsArray[1]
        lapAv = lapStatsArray[2]
        
        lapsArray =
            [[("Fastest Lap:", lapMin),
              ("Slowest Lap:", lapMax),
              ("Average Lap", lapAv)]]
        
        lapsArray.append(tempArray)
    }
    
    @IBAction func showStatsTap(_ sender: Any) {
        finalizingArray()
        performSegue(withIdentifier: "SendDataSegue", sender: self)
    }
    
    @IBAction func backButtonTap(_ sender: Any) {
        let alert = UIAlertController(title: "WAIT", message: "Do you want to go back to the launch screen?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
                   (_)in
                    self.performSegue(withIdentifier: "goBackSegue", sender: self)
                })
        alert.addAction(yesAction)
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
