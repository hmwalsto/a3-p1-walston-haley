//
//  ViewController.swift
//  A3_P1_Walston_Haley
//
//  Created by Haley Walston on 9/29/19.
//  Copyright © 2019 Haley Walston. All rights reserved.
//
//
// Assets used:
// Watercolor Running Man - Designed by Freepik
// Back arrow: https://img.icons8.com/nolan/64/000000/arrow.png


import UIKit

class V1: UIViewController {

    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = true
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func unwindToV1(segue: UIStoryboardSegue) {}
}

