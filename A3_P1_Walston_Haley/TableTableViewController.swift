//
//  TableTableViewController.swift
//  A3_P1_Walston_Haley
//
//  Created by Haley Walston on 10/1/19.
//  Copyright © 2019 Haley Walston. All rights reserved.
//
//
//      Description: The table has two sections -- Overall Stats
//      & Lap-wise stats. All stats follow format given. Nav bar
//      should be visible and toolbar hidden.
//      If the user taps back, return to V2.
//      
//

import UIKit

class TableTableViewController: UITableViewController {

   
    var sectionHeaders = ["Overall Stats", "Lap-wise Stats"]
    
    var myDataSource: [[(String, String)]] = [
        [(english: "Red", spanish: "Rojo"),
         (english: "Green", spanish: "Verde"),
         (english: "blue", spanish: "Azul")],
        
        [(english: "Tuesday", spanish: "Martes"),
         (english: "Thursday", spanish: "Jueves")]
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return myDataSource.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return myDataSource[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "My Table", for: indexPath)
        let cellData: (english: String, spanish: String) = myDataSource[indexPath[0]][indexPath[1]]
        
        
        // Configure the cell...
        cell.textLabel?.text = cellData.english
        cell.detailTextLabel?.text = "(\"\(cellData.spanish)\" in Spanish)"
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionHeaders[section]
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
